/*
  From on Paul David's tutorial : http://equalarea.com/paul/alsa-audio.html

  sudo apt-get install libasound2-dev
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include <string>
#include <alsa/asoundlib.h>

int fd, bytes;
unsigned char data[3];

const char *pDevice = "/dev/input/mice";

bool ditdah;
bool left;
bool right;

int i;
int err;
snd_pcm_t *playback_handle;
snd_pcm_hw_params_t *hw_params;
short tone[256];
short silence[256];

int len = 15;

void mouse_thread() {
	fd = open(pDevice, O_RDWR);
	if(fd == -1)
	{
		printf("ERROR Opening %s\n", pDevice);
		return;
	}

	while (true) {
		bytes = read(fd, data, sizeof(data));
		left = data[0] & 0x1;
		right = data[0] & 0x2;
	}
}

void send_dit() {
	ditdah = false;
	for (int i = len; i; --i) {
		if ((err = snd_pcm_writei (playback_handle, tone, 128)) != 128) {
			fprintf (stderr, "write to audio interface failed (%s)\n",
				 snd_strerror (err));
			exit (1);
		}
	}
	for (int i = len; i; --i) {
		if ((err = snd_pcm_writei (playback_handle, silence, 128)) != 128) {
			fprintf (stderr, "write to audio interface failed (%s)\n",
				 snd_strerror (err));
			exit (1);
		}
	}
}

void send_dah() {
	ditdah = true;
	for (int i = len*3; i; --i) {
		if ((err = snd_pcm_writei (playback_handle, tone, 128)) != 128) {
			fprintf (stderr, "write to audio interface failed (%s)\n",
				 snd_strerror (err));
			exit (1);
		}
	}
	for (int i = len; i; --i) {
		if ((err = snd_pcm_writei (playback_handle, silence, 128)) != 128) {
			fprintf (stderr, "write to audio interface failed (%s)\n",
				 snd_strerror (err));
			exit (1);
		}
	}
}
     
void send_nothing(int s) {
	for (int i = len*s; i; --i) {
		if ((err = snd_pcm_writei (playback_handle, silence, 128)) != 128) {
			fprintf (stderr, "write to audio interface failed (%s)\n",
				 snd_strerror (err));
			exit (1);
		}
	}
}

void wait() {
	if ((err = snd_pcm_writei (playback_handle, silence, 128)) != 128) {
		fprintf (stderr, "write to audio interface failed (%s)\n",
			 snd_strerror (err));
		exit (1);
	}
}

main (int argc, char *argv[])
{
	if (argc > 1) {
		len = 413/std::stoi(std::string(argv[1]));
	}

	std::thread mouse(mouse_thread);

	if ((err = snd_pcm_open (&playback_handle, "hw:0", SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
		fprintf (stderr, "cannot open audio device %s (%s)\n", 
			 argv[1],
			 snd_strerror (err));
		exit (1);
	}
	   
	if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0) {
		fprintf (stderr, "cannot allocate hardware parameter structure (%s)\n",
			 snd_strerror (err));
		exit (1);
	}
			 
	if ((err = snd_pcm_hw_params_any (playback_handle, hw_params)) < 0) {
		fprintf (stderr, "cannot initialize hardware parameter structure (%s)\n",
			 snd_strerror (err));
		exit (1);
	}

	if ((err = snd_pcm_hw_params_set_access (playback_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
		fprintf (stderr, "cannot set access type (%s)\n",
			 snd_strerror (err));
		exit (1);
	}

	if ((err = snd_pcm_hw_params_set_format (playback_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) {
		fprintf (stderr, "cannot set sample format (%s)\n",
			 snd_strerror (err));
		exit (1);
	}

	unsigned int sr = 44100;
	if ((err = snd_pcm_hw_params_set_rate_near (playback_handle, hw_params, &sr, NULL)) < 0) {
		fprintf (stderr, "cannot set sample rate (%s)\n",
			 snd_strerror (err));
		exit (1);
	}

	if ((err = snd_pcm_hw_params_set_channels (playback_handle, hw_params, 2)) < 0) {
		fprintf (stderr, "cannot set channel count (%s)\n",
			 snd_strerror (err));
		exit (1);
	}

	if ((err = snd_pcm_hw_params (playback_handle, hw_params)) < 0) {
		fprintf (stderr, "cannot set parameters (%s)\n",
			 snd_strerror (err));
		exit (1);
	}

	snd_pcm_hw_params_free (hw_params);

	if ((err = snd_pcm_prepare (playback_handle)) < 0) {
		fprintf (stderr, "cannot prepare audio interface for use (%s)\n",
			 snd_strerror (err));
		exit (1);
	}

	for (int i = 0; i < 256; ++i) {
		tone[i] = 32760 * sin(float(i) / 64.0f * M_PI);
		silence[i] = 0;
		//tone[i] = 32760;
		//tone[i] = 32760 * ((i%64 > 32) ? 1 : -1);
	}

	while (true) {   
		if (left) {
			if (right) {
				if (ditdah) {
					send_dit();
				} else {
					send_dah();
				}
			} else {
				send_dah();
			}
		} else {
			if (right) {
				send_dit();
			} else {
				wait();
			}
		}
	}

	snd_pcm_close (playback_handle);
	exit (0);
}
